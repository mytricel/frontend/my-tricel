'use client'

import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/navigation';
import { useAuth } from '@/context/AuthContext';


const Navbar: React.FC = () => {
    const { user, logout } = useAuth();
    const router = useRouter();    

  const handleLogout = () => {
    router.push('/');    
  };

  return (
    <div className="w-1/5 bg-[#23415b] h-full flex flex-col items-center justify-between py-10">
      <div className="text-lg font-bold mb-10">{user?.email}</div>
      <nav className="flex flex-col space-y-4">
        <Link href="/home">
          Home
        </Link>
        <Link href="/votes">
          Votar
        </Link>
        <Link href="/ceales">
          Ceales
        </Link>
      </nav>
      <button onClick={handleLogout} className="mt-10 text-3xl">
        ⬅️
      </button>
    </div>
  );
};

export default Navbar;
