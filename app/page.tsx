'use client'

import React, { useState, ChangeEvent, FormEvent } from 'react';
import { Dialog, Transition } from '@headlessui/react';
import { Fragment } from 'react';
import { useRouter } from 'next/navigation';
import { useAuth } from '@/context/AuthContext';

const Page: React.FC = () => {
  const { login, logout } = useAuth();
  const [isOpen, setIsOpen] = useState(false);
  const [registerForm, setRegisterForm] = useState({
    name: '',
    email: '',
    password: '',
    role: 'admin',
  });
  const [loginForm, setLoginForm] = useState({
    email: '',
    password: '',
  });
  const [error, setError] = useState<string | null>(null);
  const [errorOpen, setErrorOpen] = useState(false);
  const router = useRouter();

  const API_BASE_URL = process.env.NEXT_PUBLIC_API_BASE_URL;

  const handleRegisterChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setRegisterForm({ ...registerForm, [name]: value });
  };

  const handleRegisterSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    try {
      const res = await fetch(`${API_BASE_URL}/auth/register`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(registerForm),
      });
      if (res.ok) {        
        const data = await res.json();
        window.alert("Usuario registrado con exito");
        location.reload();
      } else {
        const data = await res.json();
        setError(data.message || 'Error registering user');
        setErrorOpen(true);
      }
    } catch (error) {
      setError('Error registering user');
      console.log("xdddd", error);
      setErrorOpen(true);
    }
  };

  const handleLoginChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setLoginForm({ ...loginForm, [name]: value });
  };

  const handleLoginSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    try {
      const res = await fetch(`${API_BASE_URL}/auth/login`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(loginForm),
      });
      if (res.ok) {        
        const data = await res.json();        
        login(data.token);
        router.push('/home');
      } else {
        const data = await res.json();
        setError(data.message || 'Error logging in user');
        setErrorOpen(true);
      }
    } catch (error) {
      setError('Error logging in user');
      setErrorOpen(true);
    }
  };

  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }

  function closeErrorModal() {
    setErrorOpen(false);
  }

  return (
    <div className="flex justify-between w-full h-screen bg-[#ffffff] text-[#fafafa]">
      <div className="bg-[#23415b] p-10 w-2/5 flex flex-col items-center justify-center text-center">
        <h2 className="text-2xl mb-4">Bienvenido</h2>
        <p className="mb-4">Ya tienes una cuenta con nosotros, ingresa para votar</p>
        <button className="mt-4 px-4 py-2 bg-[#db6e59] border border-black rounded" onClick={openModal}>
          Iniciar Sesión
        </button>
      </div>
      <div className="p-10 w-3/5 flex flex-col items-center justify-center">
        <h2 className="text-black text-2xl mb-4">Crear Cuenta</h2>
        <form className="flex flex-col w-4/5" onSubmit={handleRegisterSubmit}>
          <input
            type="text"
            name="name"
            placeholder="Nombre"
            value={registerForm.name}
            onChange={handleRegisterChange}
            className="mb-4 p-2 border border-gray-300 rounded text-black"
          />
          <input
            type="email"
            name="email"
            placeholder="Email"
            value={registerForm.email}
            onChange={handleRegisterChange}
            className="mb-4 p-2 border border-gray-300 rounded text-black"
          />
          <input
            type="password"
            name="password"
            placeholder="Contraseña"
            value={registerForm.password}
            onChange={handleRegisterChange}
            className="mb-4 p-2 border border-gray-300 rounded text-black"
          />
          <div className="flex justify-around mb-4">
            <label className="flex items-center text-black">
              <input
                type="radio"
                name="role"
                value="estudiante"
                checked={registerForm.role === 'estudiante'}
                onChange={handleRegisterChange}
                className="mr-2"
              />
              Estudiante
            </label>
            <label className="flex items-center text-black">
              <input
                type="radio"
                name="role"
                value="admin"
                checked={registerForm.role === 'admin'}
                onChange={handleRegisterChange}
                className="mr-2"
              />
              Admin
            </label>
          </div>
          <button type="submit" className="inline-block px-4 py-2 bg-[#db6e59] border border-black rounded-full">
            Registrarse
          </button>
        </form>
      </div>
      
      {/* Login Modal */}
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={closeModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                  <Dialog.Title as="h3" className="text-lg font-medium leading-6 text-gray-900">
                    Iniciar Sesión
                  </Dialog.Title>
                  <div className="mt-2">
                    <form className="flex flex-col" onSubmit={handleLoginSubmit}>
                      <input
                        type="email"
                        name="email"
                        placeholder="Email"
                        value={loginForm.email}
                        onChange={handleLoginChange}
                        className="mb-4 p-2 border border-gray-300 rounded text-black"
                      />
                      <input
                        type="password"
                        name="password"
                        placeholder="Contraseña"
                        value={loginForm.password}
                        onChange={handleLoginChange}
                        className="mb-4 p-2 border border-gray-300 rounded text-black"
                      />
                      <button type="submit" className="px-4 py-2 bg-[#23415b] text-white rounded">
                        Ingresar
                      </button>
                    </form>
                  </div>
                  <div className="mt-4">
                    <button
                      type="button"
                      className="inline-flex justify-center rounded-md border border-transparent bg-red-600 px-4 py-2 text-sm font-medium text-white hover:bg-red-700 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2"
                      onClick={closeModal}
                    >
                      Cerrar
                    </button>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>

      {/* Error Modal */}
      <Transition appear show={errorOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={closeErrorModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                  <Dialog.Title as="h3" className="text-lg font-medium leading-6 text-gray-900">
                    Error
                  </Dialog.Title>
                  <div className="mt-2">
                    <p className="text-sm text-gray-500">
                      {error}
                    </p>
                  </div>
                  <div className="mt-4">
                    <button
                      type="button"
                      className="inline-flex justify-center rounded-md border border-transparent bg-red-600 px-4 py-2 text-sm font-medium text-white hover:bg-red-700 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2"
                      onClick={closeErrorModal}
                    >
                      Cerrar
                    </button>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </div>
  );
};

export default Page;
