import React from 'react';
import Navbar from '@/components/navbar';

const Home: React.FC = () => {    

  return (
    <div className="flex h-screen">
      <Navbar/>
      <div className="w-4/5 bg-cover bg-center" style={{ backgroundImage: "url('/images/background.png')" }}>        
      </div>
    </div>
  );
};

export default Home;
